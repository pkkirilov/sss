from sss import Index


def menu_selector() -> None:
    '''
    Interactive GUI for the user to try out the functionality:
    - record trades;
    - calculate dividends
    - calculate P/E ratios
    - calculate geometric mean of all members of index
    - calculate the volume weighted stock price
    '''
    print('** Make your selection by pressing the associated number, then enter **\n')
    print('1 - Calculate dividend')
    print('2 - Calculate P/E Ratio')
    print('3 - Add trade')
    print('4 - Calculate volume weighted stock for the last 15 minutes')
    print('5 - Calculate GBCE of all shares')
    print('6 - Quit or Ctrl + C')


if __name__ == '__main__':
    idx = Index()
    while True:
        menu_selector()
        chosen_option = input('Select option: ')
        try:
            if chosen_option in ['1', '2', '3', '4']:
                ticker = input('Select a ticker: ')
                idx.check_member(ticker)
                if chosen_option in '12':
                    price = float(input('Select a price: '))
                    idx.check_price(price)
                    if chosen_option == '1':
                        print(f'Dividend is {idx.get_div(ticker, price)} \n')
                    else:
                        print(f'P/E ratio is {idx.get_pe_ratio(ticker, price)} \n')
                elif chosen_option == '3':
                    quantity = int(input('Select quantity: '))
                    buy = input('Is this a buy order? (Y) or (N): ').upper()
                    buy = True if buy == 'Y' else False
                    idx.record_trade(ticker, quantity, buy)
                else:
                    print(f'The Volume Weighted Stock for the last 15 mins is {idx.get_volume_weight_price(ticker)}.\n')
            elif chosen_option == '5':
                print(f'The GBCE All Share Index is {idx.get_geo_mean()}.\n')
            elif chosen_option == '6':
                print('Quitting!')
                break
            else:
                print('Invalid option!\n')
        except (KeyError, ValueError, Exception) as e:
            print(f'Error encountered, Details: {e}')
