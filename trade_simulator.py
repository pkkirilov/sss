import time

from sss import Index

def trade_sim() -> None:
    '''
    Simulate 25 trades in order to test out the functionality
    of getting the volume weighted price and also the 
    geometric mean of all the members in the Index.
    '''
    idx = Index()
    volumes = [5, 16, 25, 36, 45]
    for _ in range(5):
        i = len(volumes) - 1
        for stock in idx.members.keys():
            quantity = volumes[i]

            # if quantity mod 2 has remainder -> True, else False
            # in order to make trades a bit different
            buy = False
            if quantity % 2:
                buy = True
            idx.record_trade(stock, quantity, buy)
            i -= 1
            time.sleep(0.5)
   
    vwp_gin = idx.get_volume_weight_price('GIN')
    vwp_joe = idx.get_volume_weight_price('JOE')
    geo_mean = idx.get_geo_mean()
    ans = f'''Volume weight price of:\nGIN -> {vwp_gin}\nJOE -> {vwp_joe}
GBCE All Share Index geometric mean: {geo_mean}'''
    return ans


if __name__ == '__main__':
    print(trade_sim())
    