import datetime

from meta import TradeMeta, StockMeta

class Index:
    '''Index class of the GBCE members and calculation methods.'''

    def __init__(self) -> None:
        mems = [
    ['TEA', 'Common', 0, None, 100], 
    ['POP', 'Common', 8, None, 100], 
    ['ALE', 'Common', 23, None, 60], 
    ['GIN', 'Preferred', 8, 0.02, 100], 
    ['JOE', 'Common', 13, None, 250]
    ]
        self.trade_book = {}
        self.members = {m[0]: StockMeta(*m[1:]) for m in mems}


    def check_member(self, stock: str) -> bool:
        '''
        Checks to see if stock is part of the members of the index.
        
        :param stock: str abbreviation of stock
        :return: boolean
        '''
        if stock not in self.members:
            raise ValueError(f'Ticker {stock} not in index members.')
        return True

    
    def check_price(self, price: float) -> bool:
        '''
        Checks to see if the price is positive and greater than zero.
        
        :param price: stock price
        :return: boolean
        '''
        if price == 0:
            raise ValueError(f'Entered price {price} not valid.')
        return True


    def record_trade(self, stock: str, quantity: int, buy: bool=True) -> None:
        '''
        Records trades in the trade book for further analysis.
        
        :param stock: str abbreviation of stock
        :param quantity: quantity of shares
        :param buy: if trade is for purchasing or selling of stock
        '''
        t = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
        self.trade_book[t] = TradeMeta(stock, buy, quantity, self.members[stock].value)


    def get_geo_mean(self) -> float:
        '''
        Calculates the Index by using the geometric mean of prices of all stocks.
        
        :return: geometric mean of traded members'''
        if not len(self.trade_book):
            raise ValueError('No trades in tradebook.')
        tot = 1
        for trade in self.trade_book.values():
            tot *= trade.price
        return tot**(1/len(self.trade_book))


    def get_div(self, stock: str, price: float) -> float:
        '''
        Calculates the dividend yield of a stock.
        
        :param stock: str abbreviation of stock
        :param price: stock price
        :return: dividend yield
        '''
        member = self.members[stock]
        if member.type == 'Common':
            return member.last_div / price
        return member.fixed_div * member.value / price


    def get_pe_ratio(self, stock: str, price: float) -> float:
        '''
        Calculates the P/E ratio, given a stock and a price 
        depending on the stock type.
        
        :param stock: str abbreviation of stock
        :param price: stock price
        :return: P/E ratio
        '''
        div = 1
        if self.get_div(stock, price):
            div = self.get_div(stock, price)
        return price / div


    def get_volume_weight_price(self, stock: str) -> float:
        '''
        Calculates the volume weighted stock price on trades in the last 15 mins.

        :param stock: str abbreviation of stock
        :return: volume weighted stock price
        '''
        fifteen_min_int = (
            datetime.datetime.now() - datetime.timedelta(minutes=15)
            ).strftime('%Y%m%d%H%M%S%f')
        total = 0
        cnt = 0
        for t, trade in self.trade_book.items():
            if t >= fifteen_min_int and trade.ticker == stock:
                total = trade.quantity * trade.price
                cnt += trade.quantity
        else:
            # only in the case when the stock hasn't traded in the last 15 mins
            cnt = 1
        return total / cnt
            