import unittest

from sss import Index

class Testsss(unittest.TestCase):
    '''Collection of tests for the sss.py module.'''

    def test_check_member(self):
        '''Test the logic of the check_member.'''
        assert Index().check_member('GIN') == True
        with self.assertRaises(ValueError):
            Index().check_member('TONIC')

    
    def test_check_price(self):
        '''Test the logic of the check_price.'''
        with self.assertRaises(ValueError):
            assert Index().check_price(0)


    def test_get_div(self):
        '''Test the dividend yield logic.'''
        assert Index().get_div('GIN', 1) == 2.0
        assert Index().get_div('JOE', 100) == 0.13


    def test_get_pe_ratio(self):
        '''Test the P/E ratio logic.'''
        assert Index().get_pe_ratio('GIN', 10) == 50.0


    def test_empty_geo_mean(self):
        with self.assertRaises(ValueError):
            assert Index().get_geo_mean()

    
if __name__ == '__main__':
    import doctest
    doctest.testmod()
    unittest.main(verbosity=2)
