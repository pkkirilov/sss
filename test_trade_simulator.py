import unittest

from trade_simulator import trade_sim

expected_simulation_response = '''Volume weight price of:
GIN -> 1600.0
JOE -> 1250.0
GBCE All Share Index geometric mean: 108.44717711976988'''

class TestTradeSim(unittest.TestCase):
    '''Collection of tests for the trade_simulator.py module.'''
    
    def test_trade_sim(self):
        resp = trade_sim()
        assert resp == expected_simulation_response

    
if __name__ == '__main__':
    import doctest
    doctest.testmod()
    unittest.main(verbosity=2)
