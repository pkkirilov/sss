from typing import NamedTuple, Union

class StockMeta(NamedTuple):
    '''Stock metadata class.'''

    type: str
    last_div: float
    fixed_div: Union[float, None]
    value: float


class TradeMeta(NamedTuple):
    '''Trade book metadata class.'''

    ticker: str
    buy_indic: bool
    quantity: int
    price: float
    
