# SSS
Super Simple Stock market application 

### Requirements
Produce working code that will:
- For a given ticker:
    - Given any price as input, calculate the dividend yield
    - Given any price as input, calculate the P/E Ratio
    - Record a trade, with timestamp, quantity of shares, buy or sell indicator and traded price
    - Calculate Volume Weighted Stock Price based on trades in past 15 minutes
- Calculate the GBCE All Share Index using the geometric mean of prices for all stocks

### Sample data
| Ticker 	| Type      	| Last Div 	| Fixed Div 	| Par Val 	|
|--------	|-----------	|----------	|-----------	|---------	|
| TEA    	| Common    	| 0        	|           	| 100     	|
| POP    	| Common    	| 8        	|           	| 100     	|
| ALE    	| Common    	| 23       	|           	| 60      	|
| GIN    	| Preferred 	| 8        	| 2%        	| 100     	|
| JOE    	| Common    	| 13       	|           	| 250     	|

### Deployment
Run `python sss_gui.py` in order to launch the primitive GUI - a user can interact, record trades, calculate ratios ect. \
Run `python trade_simulator.py` in order to simulate 25 trades and get the Volume Weighted Price of 'GIN' and 'JOE' and get the GBCE All Share Index using the geometric mean

### Testing
Run `python test_sss.py` for the business logic in `sss.py` \
Run `python test_trade_simulator.py` for the trade simulation module
